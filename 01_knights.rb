require './00_tree_node.rb'

class KnightPathFinder
  attr_accessor :start_node

  def initialize(start_position)
    @start_node = PolyTreeNode.new(start_position)
    build_move_tree
  end

  def find_path(end_point)
    if trace_path_back(@start_node.bfs(end_point))
      p trace_path_back(@start_node.bfs(end_point))
    else
      puts 'moves not found'
      nil
    end
  end

  def trace_path_back(node)
    trace = []
    current_node = node
    until current_node.parent.nil?
      trace.unshift(current_node.value)
      current_node = current_node.parent
    end
    trace.unshift(current_node.value)

    trace
  end

  def build_move_tree
    @visited_positions = [@start_node.value]
    queue = [@start_node]

    until queue.empty?
      current_node = queue.shift
      add_horizontal_moves(current_node)
      add_vertical_moves(current_node)
      queue += current_node.children
    end
  end

  def add_horizontal_moves(start)
    start_value = start.value
    current_row = start_value[0]
    current_column = start_value[1]
    [-2, 2].each do |two_step|
      [-1, 1].each do |one_step|

        x_var = current_row + two_step
        y_var = current_column + one_step

        unless out_of_bounds?(x_var, y_var)
          unless @visited_positions.include?([x_var, y_var])
            @visited_positions << [x_var, y_var]

            start.add_child(PolyTreeNode.new([x_var, y_var]))
          end
        end
      end
    end
  end

  def add_vertical_moves(start)
    start_value = start.value
    current_row = start_value[0]
    current_column = start_value[1]
    [-1, 1].each do |one_step|
      [-2, 2].each do |two_step|

        x_var = current_row + one_step
        y_var = current_column + two_step

        unless out_of_bounds?(x_var, y_var)
          unless @visited_positions.include?([x_var, y_var])
            @visited_positions << [x_var, y_var]
            start.add_child(PolyTreeNode.new([x_var, y_var]))
          end
        end
      end
    end
  end

  def out_of_bounds?(x, y)
    x < 0 || x > 7 || y < 0 || y > 7
  end


end

# example
# knight = KnightPathFinder.new([0,0])
# knight.find_path([7,6])

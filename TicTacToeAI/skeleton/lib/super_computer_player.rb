require_relative 'tic_tac_toe_node'

class SuperComputerPlayer < ComputerPlayer
  def move(game, mark)
    node = TicTacToeNode.new(game.board, mark)

    the_move = nil
    node.children.each do |child|
      if child.winning_node?(mark)
        the_move = child.prev_move_pos
        break
      end
    end

    if the_move.nil?
      node.children.each do |child|
        the_move = child.prev_move_pos unless child.losing_node?(mark)
      end
    end

    raise 'There are no non-losing nodes' if the_move.nil?

    the_move
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "Play the brilliant computer!"
  hp = HumanPlayer.new("Jeff")
  cp = SuperComputerPlayer.new

  TicTacToe.new(hp, cp).run
end

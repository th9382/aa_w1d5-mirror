require_relative 'tic_tac_toe'

class TicTacToeNode
  attr_accessor :board, :next_mover_mark, :prev_move_pos
  def initialize(board, next_mover_mark, prev_move_pos = nil)
    @board = board
    @next_mover_mark = next_mover_mark
    @prev_move_pos = prev_move_pos

  end

  def losing_node?(evaluator)
    if @board.over?
      if @board.winner == opposite(evaluator)
        return true
      end

      if @board.winner == nil || @board.winner == evaluator
        return false
      end
    else

    return true if @next_mover_mark == evaluator &&
      !children.empty? &&
      children.all? do |child|
        child.losing_node?(evaluator)
      end

    return true if @next_mover_mark == opposite(evaluator) &&
      !children.empty? &&
      children.any? do |child|
        child.losing_node?(evaluator)
      end
    end

    false
  end

  def winning_node?(evaluator)

    if @board.over?
      if @board.winner == evaluator
        return true
      end

      if @board.winner == nil || @board.winner == opposite(evaluator)
        return false
      end
    else

      return true if @next_mover_mark == evaluator &&
        !children.empty? &&
        children.any? do |child|
          child.winning_node?(evaluator)
      end

      return true if @next_mover_mark == opposite(evaluator) &&
        !children.empty? &&
        children.all? do |child|
          child.winning_node?(evaluator)
      end
    end

    false
  end

  def opposite(move)
    if move == :x
      return :o
    else
      return :x
    end
  end

  # This method generates an array of all moves that can be made after
  # the current move.
  def children
    children = []

    @board.rows.each_with_index do |row, row_idx|
      row.each_with_index do |col, col_idx|
        next unless col.nil?
        clone_board = @board.dup
        clone_board[[row_idx, col_idx]] = @next_mover_mark
        new_tic_tac = TicTacToeNode.new(clone_board,
                                        opposite(@next_mover_mark),
                                        [row_idx, col_idx])
        children << new_tic_tac
      end
    end

    children
  end
end

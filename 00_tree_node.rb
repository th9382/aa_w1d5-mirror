class PolyTreeNode
  attr_reader :parent, :children, :value

  def initialize(value)
    @value = value
    @parent = nil
    @children = []
  end

  def parent=(parent_node)
    parent.children.delete(self) unless parent.nil?

    @parent = parent_node

    unless parent_node.nil?
      parent_node.children << self unless parent_node.children.include?(self)
    end
  end

  def add_child(child_node)
    child_node.parent = self unless child_node == parent
  end

  def remove_child(child_node)
    raise 'not a child' unless self.children.include?(child_node)
    child_node.parent = nil
  end

  def dfs(target_value)
    return self if target_value == self.value

    children.each do |child|
      result = child.dfs(target_value)

      return result if result
    end

    nil
  end

  def bfs(target_value)
    queue = [self]

    until queue.empty?
      current_node = queue.shift
      queue += current_node.children

      return current_node if target_value == current_node.value
    end

    nil
  end
end
